// Scilab Computer Vision Module
// Copyright (C) 2017 - Scilab Enterprises
// Copyright (C) 2025 - Dassault Systèmes S.E. - Vincent COUVERT

README of Scilab Computer Vision Module
----------------------------------------

This toolbox is a binding from the OpenCV library (in version 4.8.1).
Please see www.opencv.org for further details on the OpenCV library.

This toolbox version is the same as OpenCV version.

To load and initialize the toolbox:
- cd <path to the toolbox folder>
- exec loader.sce
- scicv_Init()
