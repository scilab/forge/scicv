<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab Computer Vision Module
 * Copyright (C) 2017 - Scilab Enterprises
 *
 -->
<refentry xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab"
  xml:lang="fr" xml:id="cornerHarris">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>
  <refnamediv>
    <refname>cornerHarris</refname>
    <refpurpose>Detects the corners in a image</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Syntax</title>
    <synopsis>img_out = cornerHarris(img, blockSize, ksize, k[, borderType])</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>img</term>
        <listitem>
          <para>Image (<link linkend="Mat">Mat</link>).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>blockSize</term>
        <listitem>
          <para>Neighborhood size (double).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>ksize</term>
        <listitem>
          <para>Aperture parameter using in the derivatives computing (double).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>k</term>
        <listitem>
          <para>Harris detector free parameter (double).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>borderType</term>
        <listitem>
          <para>Pixel extrapolation method (double) (default <literal>BORDER_DEFAULT</literal>).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>img_out</term>
        <listitem>
          <para>Image containing the detector response (<link linkend="Mat">Mat</link>).</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para><function>cornerHarris</function> detects the corners in an image using the Harris edge detector.</para>
    <para>Corners in the image can be found as the local maxima of the response image.</para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
    scicv_Init();

    img_gray = imread(getSampleImage("blobs.jpg"), CV_LOAD_IMAGE_GRAYSCALE);

    img_cornerHarris = cornerHarris(img_gray, 7, 5, 0.05);

    img_cornerHarris_norm = normalize(img_cornerHarris, 0, 255, NORM_MINMAX, CV_32FC1, []);
    img_corners = convertScaleAbs(img_cornerHarris_norm);

    matplot(img_corners);

    delete_Mat(img_gray);
    delete_Mat(img_cornerHarris);
    delete_Mat(img_cornerHarris_norm);
    delete_Mat(img_corners);
 ]]></programlisting>
  </refsection>
  <refsection>
    <title>See also</title>
    <simplelist type="inline">
      <member><link linkend="findContours">findContours</link></member>
    </simplelist>
  </refsection>
</refentry>
