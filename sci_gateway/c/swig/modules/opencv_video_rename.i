// Scilab Computer Vision Module
// Copyright (C) 2017 - Scilab Enterprises
// Copyright (C) 2025 - Dassault Systèmes S.E. - Vincent COUVERT

%rename (KalmFltr) KalmanFilter;
%rename (BackgrdSub) BackgroundSubtractor;
%rename (BackgrdSubMOG) BackgroundSubtractorMOG;
%rename (BackgrdSubMOG2) BackgroundSubtractorMOG2;
%rename (BackgrdSubGMG) BackgroundSubtractorGMG;

%rename (measurMtx) measurementMatrix;
%rename (errCovPost) errorCovPost;
