// Scilab Computer Vision Module
// Copyright (C) 2017 - Scilab Enterprises
// Copyright (C) 2025 - Dassault Systèmes S.E. - Vincent COUVERT

%{
#undef SKIP_INCLUDES
#include "opencv2/rgbd/depth.hpp"
%}

%include opencv_contrib_ignore.i

%include "opencv2/rgbd/depth.hpp" // RIGID_BODY_MOTION, ...