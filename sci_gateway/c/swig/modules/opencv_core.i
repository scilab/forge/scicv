// Scilab Computer Vision Module
// Copyright (C) 2017 - Scilab Enterprises
// Copyright (C) 2025 - Dassault Systèmes S.E. - Vincent COUVERT

%{
#undef SKIP_INCLUDES
#undef REAL
#undef Rhs
#undef round
#include  "opencv2/core/types_c.h"
#include "opencv2/core.hpp"
#include "opencv2/core/cuda.hpp" // cv::cuda::GpuMat
#include "opencv2/core/mat.hpp"
#include "opencv2/imgcodecs/legacy/constants_c.h"
using namespace std;
using namespace cv;
using namespace ogl;
using namespace cv::ogl;
%}

%include opencv_core_ignore.i

%include ../typemaps/opencv_typemaps.i

%apply double *OUTPUT { double *minVal };
%apply double *OUTPUT { double *maxVal };

using std::vector;

#define OPENCV_FORCE_UNSAFE_XADD // Avoid error about CV_XADD macro definition
%include "opencv2/core/hal/interface.h" // CV_8UC3, CV_16S, CV_CN_MAX, ... definition
%include "opencv2/core/cvdef.h" // CV_INLINE definition
%import "opencv2/core/cvstd.hpp" // cv::String (Needed to map cv::String as Scilab string)
%include "opencv2/imgcodecs/legacy/constants_c.h" // CV_LOAD_IMAGE_* (Useful constants)
%include "opencv2/core/types.hpp" // Point_, Size_, Rect, Scalar_, ...

%include "opencv2/core/types_c.h"
%include "opencv2/core/mat.hpp"
%include "opencv2/core.hpp"
%include "opencv2/core/base.hpp" // cubeRoot, ...
%include "opencv2/core/utility.hpp" // getCPUTickCount, ...
%include "opencv2/core/core_c.h" // cvarrToMatND, ...
%import "opencv2/core/operations.hpp"

%template() cv::Point_<int>;
%template() cv::Point_<float>;
%template() cv::Size_<int>;
%template() cv::Rect_<int>;
%template() cv::Scalar_<double>;

%include "opencv2/imgcodecs.hpp" // imread, ...

%include carrays.i

%array_functions( double, double_array )
%array_functions( float, float_array )
%array_functions( int, int_array )

%include cpointer.i

%pointer_functions(int, intp);

%include opencv_core_helpers.i
