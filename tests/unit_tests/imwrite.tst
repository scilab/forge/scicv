// Scilab Computer Vision Module
// Copyright (C) 2017 - Scilab Enterprises

// <-- CLI SHELL MODE -->

scicv_Init();

img = imread(getSampleImage("lena.jpg"));

res = imwrite(fullfile(TMPDIR, "lena_imwrite.jpg"), img);
assert_checktrue(res);

delete_Mat(img);
